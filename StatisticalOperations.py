import os.path
import StringIO
import csv
import math
from random import randint




class Statical:
    def create_list(self):
        """ create_list
          Creates a list of one thousand integers on Problem9.csv.
          If Problem9.csv exists, the file is overwritten.
        """
        dir = r"/home/devuser/PycharmProjects/statisticaloper"
        if not os.path.exists(dir):
            os.mkdir(dir)
        my_list = []

        while len(my_list) < 1000:
            num = randint(0, 100)
            my_list.append([num])

        with open(os.path.join(dir, "Problem9" + '.csv'), "w") as f:

            csvfile = StringIO.StringIO()
            csvwriter = csv.writer(csvfile)

            for l in my_list:
                csvwriter.writerow(l)
            for a in csvfile.getvalue():
                f.writelines(a)

    def get_data(self):
        """ get_data

        Returns a list of integers from Problem9.csv

        """
        file_data = open('Problem9.csv', "r")
        file_data_list = []
        for l in file_data:
            file_data_list.append(int(l))

        return file_data_list

    def get_mean(self):
        """ get_mean
          Returns the mean of a list of numbers obtained from Problem9.csv
        """
        file_data_list = self.get_data()

        if len(file_data_list) == 0:
            return 0

        additions = 0
        for num in file_data_list:
            additions += num

        return additions/len(file_data_list)

    def get_standard_deviation(self):
        """ get_standard_deviation
          Returns the standard deviation of a list of numbers obtained from Problem9.csv
        """
        file_data_list = self.get_data()
        mean = self.get_mean()
        stdAdditions = 0
        for num in file_data_list:
            stdAdditions += pow(num-mean, 2)
        var = stdAdditions/len(file_data_list)
        print math.sqrt(var)

    def get_mode(self):
        """ get_mode
          Returns the mode of a list of numbers obtained from Problem9.csv
        """
        file_data_list = self.get_data()
        return max(set(file_data_list), key=file_data_list.count)

    def get_quartiles(self):
        """ get_quartiles
          Returns the quartiles of a list of numbers obtained from Problem9.csv
        """
        file_data_list = self.get_data()
        file_data_list.sort()
        quartileDivision = round(len(file_data_list)/float(4))

        return [file_data_list[int(quartileDivision)], file_data_list[int(quartileDivision*2)], file_data_list[int(quartileDivision*3)]]

    def get_linear_regresion(self):
        """ get_linear_regresion
          Returns the linear regresion of a list of numbers obtained from Problem9.csv
        """
         file_data_list = self.get_data()
         n = len(file_data_list)
         X = 0
         Y = 0
         X2 = 0
         Y2 = 0
         XY = 0

         for points in file_data_list:
             X  += points
             Y  += points
             XY += 2*points
             X2 += points*points
             Y2 += points*points

         b = ((n * XY) - (X * Y)) / (n * X2 - X * X)
         a = (Y / n) - (b * (X / n))

         print "linear regresion Parameters: "
         print ""
         print "a: ", a
         print "b: ", b



def main():
    sts = Statical()
    print "Mean: "
    print sts.get_mean()
    print "Standard deviation: "
    print sts.get_standard_deviation()
    print "Mode: "
    print sts.get_mode()
    print "Quartiles: "
    print sts.get_quartiles()
    sts.get_linear_regresion()


# execute main
if __name__ == "__main__":
    main()